FROM python:3.7

WORKDIR /application
COPY restapi-server.tar.gz /application/
RUN tar -zxvf /application/restapi-server.tar.gz
RUN pip install -r /application/restapi-server/req.txt
EXPOSE 8080
CMD [ "python", "/application/restapi-server/web_app.py" ]
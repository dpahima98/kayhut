# RestAPI

This project create a python:3.7 docker image with zerotier restAPI 

## Installation

Use Git to clone the project 

```bash
git clone git@gitlab.com:dpahima98/docker-file.git
```

To use the docker run :

```bash
docker-compose up -d 
```

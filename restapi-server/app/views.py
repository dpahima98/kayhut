from app import app
from flask import render_template, request
from .models import UserForm
from .models import ZeroTier
import json

@app.route("/", methods = ['GET', 'POST'])
@app.route("/home", methods = ['GET', 'POST'])
def signin():
    form = UserForm()
    if request.method == "POST":
        netID = request.form["netID"]
        token = request.form["token"]
        data = ZeroTier(netID, token)
        return json.dumps(data.getIP())
    return render_template("home.html", title="Sign in", form=form)
@app.route("/status")
def status():
    return "UP"